## Cryptocurrency Price Tracker

Display real-time crypto currency price

#### Requirements

 - ##### Hardware
	 - ESP32 or ESP8266 or equivalent

 - ##### Software
	 - Arduino Websockets  
  	 ```arduino-cli lib install ArduinoWebsockets```


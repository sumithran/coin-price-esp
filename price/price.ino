#include <ESP8266WiFi.h>
#include <ArduinoWebsockets.h>
#include "Credentials.h"

char path[] = "/echo";
char host[] = "ws-ap2.pusher.com";

const char* websockets_server = "wss://ws-ap2.pusher.com/app/47bd0a9591a05c2a66db?protocol=7&client=js&version=4.4.0&flash=false";

using namespace websockets;

WebsocketsClient client;

void onMessageCallback(WebsocketsMessage message) {

    String message_data = message.data();

	// Serial.print("Got Message: ");
	// Serial.println(message_data);

	if ((message_data).indexOf("\"event\":\"trades\"") == 1) {

		int index_of_price = (message_data).indexOf("price");
		// Serial.print("indexOf price: ");
		// Serial.println(index_of_price);

		int index_of_comma = message_data.indexOf(",", index_of_price);
		// Serial.print("indexOf first comma after price: ");
		// Serial.println(index_of_comma);

		Serial.print("price: ");
		Serial.println(message_data.substring(index_of_price+10, index_of_comma-2));
	}
}

void onEventsCallback(WebsocketsEvent event, String data) {
	if(event == WebsocketsEvent::ConnectionOpened) {
		Serial.println("Connnection Opened");
	} else if(event == WebsocketsEvent::ConnectionClosed) {
		Serial.println("Connnection Closed");
	}
}

void setup() {
	Serial.begin(115200);
	// Connect to wifi
	WiFi.begin(ssid, password);

	// Wait some time to connect to wifi
	for(int i = 0; i < 10 && WiFi.status() != WL_CONNECTED; i++) {
		Serial.print(".");
		delay(1000);
	}
	// Setup Callbacks
	client.onMessage(onMessageCallback);
	client.onEvent(onEventsCallback);

	// Connect to server
	client.connect(websockets_server);

	// Send message
	client.send("{\"event\":\"pusher:subscribe\",\"data\":{\"channel\":\"market-dogeinr-global\"}}");
}

void loop() {
	client.poll();
	// delay(1000);
}